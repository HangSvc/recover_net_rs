#![windows_subsystem = "windows"]

use anyhow::Result;
use recover_net::run;

#[tokio::main]
async fn main() -> Result<()> {
    run().await?;
    Ok(())
}
