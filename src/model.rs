#[derive(Deserialize, Debug)]
pub struct LoginResponse {
    pub success: bool,
    pub msg: String,
    pub action: String,
    pub pop: i32,

    #[serde(alias = "userName")]
    pub user_name: String,
    pub location: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Login {
    pub encrypt: bool,
    pub username: String,
    pub password: String,
}

use crate::encrypt::{aes_decrypt, aes_encrypt};
use aes_gcm::{aead::KeyInit, Aes256Gcm, Key};
use anyhow::Result;
use log::info;
use serde::{Deserialize, Serialize};
use std::fs::OpenOptions;
use std::io::Write;
use tokio::fs::read_to_string;

impl Login {
    pub async fn new() -> Result<Self> {
        // 读取配置文件
        let content = read_to_string("./recover.toml").await?;
        let login_res: Login = toml::from_str(&content)?;
        info!("read ./recover.toml successfully");
        Ok(login_res)
    }

    pub async fn parse_config(&mut self) -> Result<bool> {
        // get encryptor
        let cipher = Aes256Gcm::new(Key::<Aes256Gcm>::from_slice(&self.get_key()));

        // 如果已经加密，则进行解密
        // 否则进行加密，并保存到配置文件中
        if self.encrypt {
            self.decrypt_username(&cipher)?;
            self.decrypt_password(&cipher)?;
            info!("parse data of ./recover.toml successfully");
            return Ok(false);
        }
        self.encrypt_username(&cipher)?;
        self.encrypt_password(&cipher)?;
        self.encrypt = true;

        let s = toml::to_string_pretty(&self)?;
        let mut file = OpenOptions::new().write(true).open("./recover.toml")?;
        file.write_all(s.as_bytes())?;
        info!("encrypt data of ./recover.toml successfully");
        return Ok(true);
    }

    fn encrypt_username(&mut self, aes_gcm: &Aes256Gcm) -> Result<()> {
        let username_enc = aes_encrypt(aes_gcm, &self.username.as_bytes())?;
        self.username = hex::encode(username_enc);

        Ok(())
    }

    fn encrypt_password(&mut self, aes_gcm: &Aes256Gcm) -> Result<()> {
        let passwd_enc = aes_encrypt(aes_gcm, &self.password.as_bytes())?;
        self.password = hex::encode(passwd_enc);

        Ok(())
    }

    fn decrypt_username(&mut self, aes_gcm: &Aes256Gcm) -> Result<()> {
        let dec = hex::decode(&self.username)?;
        let username_dec = aes_decrypt(aes_gcm, &dec)?;
        self.username = String::from_utf8(username_dec)?;

        Ok(())
    }

    fn decrypt_password(&mut self, aes_gcm: &Aes256Gcm) -> Result<()> {
        let dec = hex::decode(&self.password)?;
        let passwd_dec = aes_decrypt(aes_gcm, &dec)?;
        self.password = String::from_utf8(passwd_dec)?;

        Ok(())
    }

    fn get_key(&self) -> [u8; 32] {
        [
            105, 242, 249, 142, 120, 218, 98, 48, 196, 102, 32, 110, 30, 198, 117, 232, 47, 24,
            102, 85, 245, 110, 45, 144, 149, 105, 110, 169, 193, 155, 104, 138,
        ]
    }
}
