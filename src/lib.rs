pub mod encrypt;
pub mod log;
pub mod model;
pub mod request;

use crate::request::send_login_request;
use ::log::error;
use anyhow::Result;

pub async fn run() -> Result<()> {
    // 初始化log
    log::set_log()?;

    // 获取配置文件中的用户名和密码
    let mut login_res = match model::Login::new().await {
        Ok(v) => v,
        Err(e) => {
            error!("read ./recover.toml with error: {}", e.to_string());
            return Ok(());
        }
    };

    let first = match login_res.parse_config().await {
        Ok(v) => v,
        Err(e) => {
            error!("parse ./recover.toml with error: {}", e.to_string());
            return Ok(());
        }
    };

    // 第一次执行是为了加密，后续执行才会进行实际的登录
    if !first {
        match send_login_request(&login_res.username, &login_res.password).await {
            Err(e) => {
                error!("send login request with error: {}", e.to_string());
            }
            _ => {}
        };
    }

    Ok(())
}
