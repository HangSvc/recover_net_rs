use crate::encrypt;
use crate::model::LoginResponse;
use anyhow::{anyhow, Result};
use log::{error, info};
use std::time::Duration;
use std::time::{SystemTime, UNIX_EPOCH};

pub async fn send_login_request(username: &str, passwd: &str) -> Result<()> {
    const URL: &str = "http://10.17.2.6/ac_portal/login.php";

    let now = SystemTime::now().duration_since(UNIX_EPOCH)?;
    let src = format!("{}", now.as_millis());

    let enc_s = encrypt::rc4_encrypt(passwd.as_bytes(), src.as_bytes());
    let form = [
        ("opr", "pwdLogin"),
        ("userName", username),
        ("pwd", &enc_s),
        ("auth_tag", &src),
        ("rememberPwd", "0"),
    ];

    let builder = reqwest::ClientBuilder::new().timeout(Duration::from_secs(5));
    let client = builder.build()?;
    let res = match client.post(URL).form(&form).send().await {
        Ok(v) => v,
        Err(e) => {
            return Err(anyhow!(e.to_string()));
        }
    };

    let res = res.json::<LoginResponse>().await?;
    if res.success {
        info!("login successfully");
        return Ok(());
    }

    error!("login failed with error \n{}", &res.msg);
    Ok(())
}
