use std::usize;

use aes_gcm::aead::generic_array::GenericArray;
use aes_gcm::aead::Aead;
use aes_gcm::Aes256Gcm;
use anyhow::{anyhow, Result};

pub fn rc4_encrypt(passwd: &[u8], key_stream: &[u8]) -> String {
    let (mut j, mut a, mut b) = (0_usize, 0_usize, 0_usize);
    let mut c;

    let stream_len = key_stream.len();
    let passwd_len = passwd.len();

    let mut key = [0_u8; 256];
    let mut sbox = [0_u8; 256];
    let mut output = Vec::new();

    for i in 0..256 {
        key[i] = key_stream[i % stream_len];
        sbox[i] = i as u8;
    }

    for i in 0..256 {
        j = (j + sbox[i] as usize + key[i] as usize) & 255;
        sbox.swap(i, j);
    }

    for i in 0..passwd_len {
        a = (a + 1) & 255;
        b = (b + sbox[a] as usize) & 255;
        sbox.swap(a, b);
        c = (sbox[a] as usize + sbox[b] as usize) & 255;
        output.push(passwd[i] ^ sbox[c]);
    }

    hex::encode(output)
}

pub fn aes_encrypt(cipher: &Aes256Gcm, data: &[u8]) -> Result<Vec<u8>> {
    let res = match cipher.encrypt(GenericArray::from_slice(&get_nonce()), data) {
        Ok(v) => v,
        Err(e) => {
            return Err(anyhow!(e.to_string()));
        }
    };
    Ok(res)
}

pub fn aes_decrypt(cipher: &Aes256Gcm, data: &[u8]) -> Result<Vec<u8>> {
    let res = match cipher.decrypt(GenericArray::from_slice(&get_nonce()), data) {
        Ok(v) => v,
        Err(e) => {
            return Err(anyhow!(e.to_string()));
        }
    };
    Ok(res)
}

fn get_nonce() -> [u8; 12] {
    [36, 133, 71, 198, 177, 39, 54, 158, 210, 138, 176, 59]
}

#[cfg(test)]
mod tests {
    use super::*;
    use aes_gcm::{
        aead::KeyInit,
        Aes256Gcm,
        Key, // Or `Aes128Gcm`
    };

    #[test]
    fn test_rc4_encrypt() {
        let e = rc4_encrypt("111111".as_bytes(), "1706320544685".as_bytes());

        assert_eq!("3e08a1eb3e4c".to_string(), e);
    }

    #[test]
    fn aes_encrypt() {
        let message = "B0095586";
        let key = [
            105, 242, 249, 142, 120, 218, 98, 48, 196, 102, 32, 110, 30, 198, 117, 232, 47, 24,
            102, 85, 245, 110, 45, 144, 149, 105, 110, 169, 193, 155, 104, 138,
        ];

        let cipher = Aes256Gcm::new(Key::<Aes256Gcm>::from_slice(&key));

        let re = match aes_decrypt(&cipher, &message.as_bytes()) {
            Ok(v) => v,
            Err(e) => {
                println!("{e}");
                return;
            }
        };
        println!("{:#?}", &re);

        let r = hex::encode(&re);
        println!("{r}");
    }
}
